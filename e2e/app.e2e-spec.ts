import { AngularDropdownPhonePage } from './app.po';

describe('angular-dropdown-phone App', () => {
  let page: AngularDropdownPhonePage;

  beforeEach(() => {
    page = new AngularDropdownPhonePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
