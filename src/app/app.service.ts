import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Country } from './models/country.interface';

const COUNTRY_API = 'https://restcountries.eu/rest/v2/all';

@Injectable()
export class CountryService {
  constructor(private http: Http) {}

  getAllCountry(): Observable<Country[]> {
    return this.http.get(COUNTRY_API).map((response: Response) => {
      return response.json().map(item => {
        return {
          name: item.name,
          topLevelDomain: item.topLevelDomain[0].replace('.', ''),
          code: item.callingCodes[0]
        };
      });
    });
  }
}
