import { Component, OnInit, HostListener, ElementRef } from '@angular/core';

import { CountryService } from './app.service';

import { Country } from './models/country.interface';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css', './sprite.css'],
  template: `
    <div class="wrapper">
      <div class="country" (click)="toggleCodeList()">
        <span class="flag flag-16 flag-{{ country && country[selectedCountryIndex].topLevelDomain }}"></span>
        <span class="plus">+</span>
      </div>

      <input
        type="text"
        class="country-code"
        [value]="country && country[selectedCountryIndex].code"
        (input)="onInputCode(code.value)"
        #code
      >
      <input type="number" class="number">
      <ul [class.hide]="isListClose" class="country-code-list">
        <li
          *ngFor="let item of country; let i = index"
          (click)="selectCountry(i)"
         
        >
          <span class="flag flag-16 flag-{{item.topLevelDomain}}"></span>
          {{ item.name }} <span class="gray">+{{ item.code}}</span>
        </li>
      </ul>
    </div>
  `
})
export class AppComponent implements OnInit {
  title = 'app';
  isListClose: boolean = true;
  selectedCountryIndex: number = 0;

  country: Country[];

  constructor(private county: CountryService, private eRef: ElementRef) {}

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.toggleCodeList();
    }
  }

  ngOnInit() {
    this.county.getAllCountry().subscribe((data: Country[]) => {
      this.country = data;
    });
  }

  toggleCodeList() {
    this.isListClose = !this.isListClose;
  }

  selectCountry(index: number) {
    this.selectedCountryIndex = index;
    this.toggleCodeList();
  }

  onInputCode(code: string) {
    let index = this.country.findIndex((country: Country) => {
      if (country.code === code) console.log(country, code);
      return country.code === code;
    });
    if (index !== -1) this.selectedCountryIndex = index;
  }
}
