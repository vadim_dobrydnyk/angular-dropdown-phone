export interface Country {
  name: string;
  topLevelDomain: string;
  code: string;
}
